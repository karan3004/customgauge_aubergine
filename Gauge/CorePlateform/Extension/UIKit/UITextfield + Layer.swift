//
//  UITextfield + Layer.swift
//  GaugeDemo
//
//  Created by Aubergine Admin on 05/11/20.
//

import Foundation
import UIKit


extension UITextField {
     func addToolbar() {
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(resetTapped))
        bar.items = [reset]
        bar.sizeToFit()
        self.inputAccessoryView = bar
    }
    @objc func resetTapped() {
        self.resignFirstResponder()
    }
}
