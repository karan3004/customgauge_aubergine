//
//  UIView + Layer.swift
//  GaugeDemo
//
//  Created by Aubergine Admin on 02/11/20.
//

import Foundation
import UIKit

extension UIView {
    public func setCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}
