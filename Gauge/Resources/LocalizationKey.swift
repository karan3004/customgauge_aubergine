//
//  LocalizationKey.swift
//  Gauge
//
//  Created by Aubergine Admin on 09/11/20.
//

import Foundation


enum LocalizationKey: String {
    
    /*
     ** Main screen
     */
    case get_competition
    case start_date
    case end_date
    case day_left
    case start_date_title
    case end_date_title
    case percentage
    case lb_weight
}
struct Localization {
    let key: LocalizationKey
    let string: String
    
    private init(string: String, key: LocalizationKey) {
        self.string = string
        self.key = key
    }
    
    static func set(key: LocalizationKey) -> String {
        let localized = NSLocalizedString(key.rawValue, comment: "")
        return String(format: localized)
    }
    
    static func setWithArguments(key: LocalizationKey, arguments: [CVarArg]) -> String {
        return String(format: NSLocalizedString(key.rawValue, comment: ""), arguments: arguments)
    }
}
