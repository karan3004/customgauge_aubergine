//
//  ViewController.swift
//  Gauge
//
//  Created by Aubergine Admin on 07/11/20.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var viewSemicircle: UIView! {
        didSet {
            viewSemicircle.setCornerRadius(radius: 20.0)
        }
    }
    @IBOutlet weak var btnSubmit: UIButton! {
        didSet {
            btnSubmit.setCornerRadius(radius: 5.0)
        }
    }
    @IBOutlet weak var viewDaysleft: UIView! {
        didSet {
            viewDaysleft.setCornerRadius(radius: 10.0)
        }
    }
    @IBOutlet var txtValue: UITextField!
    @IBOutlet weak var viewGauge: GaugeView!
    @IBOutlet weak var lblCompetition: UILabel!
    @IBOutlet weak var lblStartdate: UILabel!
    @IBOutlet weak var lblStartdatetitle: UILabel!
    @IBOutlet weak var lblEnddate: UILabel!
    @IBOutlet weak var lblEnddatetitle: UILabel!
    @IBOutlet weak var lblLb: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblDaysleft: UILabel!
    
    //Labels Properties
    var lblsuperFont = UIFont.systemFont(ofSize: 14)
    var lblColor = UIColor.white
    var lblLbcolor = UIColor(red: 0.635, green:  0.847, blue:  0.213, alpha: 1.0)
    
    //MARK:- ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        // Do any additional setup after loading the view
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        viewGauge.makeSemiCircle(animationValue: 0.5)
        viewGauge.indicatorValue = 0.5
    }
    
    func setUpUI() {
        lblStartdate.text = Localization.set(key: .start_date)
        lblCompetition.text = Localization.set(key: .get_competition)
        lblStartdatetitle.text = Localization.set(key: .start_date_title)
        lblEnddate.text = Localization.set(key: .end_date)
        lblEnddatetitle.text = Localization.set(key: .end_date_title)
        lblDaysleft.text = Localization.set(key: .day_left)
        
        lblStartdate.textColor = lblColor
        lblEnddate.textColor = lblColor
        lblStartdatetitle.textColor = lblColor
        lblEnddatetitle.textColor = lblColor
        lblCompetition.textColor = lblColor
        lblDaysleft.textColor = lblColor
        lblLb.textColor = lblLbcolor
        lblPercentage.textColor = lblLbcolor
        
        let attStringper:NSMutableAttributedString = NSMutableAttributedString(string:  Localization.set(key: .percentage), attributes: nil)
        attStringper.setAttributes([.font:lblsuperFont,.baselineOffset:5], range: NSRange(location:4,length:1))
        lblPercentage.attributedText = attStringper
        
        let attStringlb:NSMutableAttributedString = NSMutableAttributedString(string:  Localization.set(key: .lb_weight), attributes: nil)
        attStringlb.setAttributes([.font:lblsuperFont,.baselineOffset:5], range: NSRange(location:4,length:2))
        lblLb.attributedText = attStringlb
        
        txtValue.addToolbar()
    }
    //MARK: Button Action
    @IBAction func btnSubmitclick(_ sender: UIButton){
        if self.txtValue.text == ""{
            return
        }
        
        if let floatValue = NumberFormatter().number(from: self.txtValue.text!) {
            
            let animationValue = CGFloat(truncating: floatValue)
            
            if animationValue > 10.0 || animationValue < 0.0{
                return
            }
            
            viewGauge.makeSemiCircle(animationValue: animationValue / 10.0)
            viewGauge.indicatorValue = animationValue / 10.0
            self.view.endEditing(true)
        }
    }
    
}

