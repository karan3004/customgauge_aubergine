//
//  GaugeView.swift
//  GaugeDemo

import UIKit

class GaugeView: UIView {
    
    
    //CAShapelayer Properties
    var scaleCenter : CGPoint = CGPoint.zero
    var radius: CGFloat = 0
    var baseSemiCircleLayer   = CAShapeLayer()
    var indicatorCircleLayer   = CAShapeLayer()
    
    var outerBezelWidth: CGFloat = 10
    var innerBezelWidth: CGFloat = 5
    
    //Indicator Image
    var needleImage: UIImageView = UIImageView.init(image: #imageLiteral(resourceName: "arrow"))
    
    //Marjor/Minor Ticks Properties
    var majorTickColor = UIColor.white
    var majorTickWidth: CGFloat = 3
    var majorTickLength: CGFloat = -9
    var minorTickColor = UIColor.white.withAlphaComponent(0.5)
    var minorTickWidth: CGFloat = 2
    var minorTickLength: CGFloat = -4
    var minorTickCount = 4
    var segmentWidth: CGFloat = 15
    var segmentCount = 10
    var totalAngle: CGFloat = 180
    var rotation: CGFloat = 270
    
    //Display lables
    let value0 = UILabel()
    let value5 = UILabel()
    let value10 = UILabel()
    
    //Labels Properties
    var valueFont = UIFont.systemFont(ofSize: 15)
    var valueColor = UIColor(red: 0.945, green:  0.670, blue:  0.282, alpha: 1.0)
    
    //Semi Circle Colors
    let circleUnfillColor = UIColor(red: 0.75, green:  0.75, blue:  0.75, alpha: 1.0)
    let circleFillColor = UIColor(red: 0.945, green:  0.670, blue:  0.282, alpha: 1.0)
    
    var indicatorValue: CGFloat = 0 {
        didSet {
            
            // figure out where the needle is, between 0 and 1
            let needlePosition = indicatorValue
            
            // create a lerp from the start angle (rotation) through to the end angle (rotation + totalAngle)
            let lerpFrom = rotation
            let lerpTo = rotation + totalAngle
            
            // lerp from the start to the end position, based on the needle's position
            let needleRotation = lerpFrom + (lerpTo - lerpFrom) * needlePosition
            needleImage.transform = CGAffineTransform(rotationAngle: deg2rad(needleRotation))
            
            //Set indicator according to animation value
            setIndicator()
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scaleCenter = CGPoint (x: self.frame.size.width / 2, y: self.frame.size.height )
        radius = (((frame.width - segmentWidth) / 2) - outerBezelWidth) - innerBezelWidth
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        guard let ctx = UIGraphicsGetCurrentContext() else { return }
        drawTicks(in: rect, context: ctx)
    }
    
    
    //MARK:- Setup UI
    func setUpUI() {
        
        value0.font = valueFont
        value0.textColor = valueColor
        value0.text = "0"
        value0.translatesAutoresizingMaskIntoConstraints = false
        if !self.subviews.contains(value0){
            self.addSubview(value0)
        }
        
        NSLayoutConstraint.activate([
            value0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            value0.leftAnchor.constraint(equalTo: leftAnchor, constant: 30)
        ])
        
        value5.font = valueFont
        value5.textColor = valueColor
        value5.text = "5"
        value5.translatesAutoresizingMaskIntoConstraints = false
        
        if !self.subviews.contains(value5){
            self.addSubview(value5)
        }
        
        let divConstant = self.frame.size.width / 2
        let segmentRadius = (((frame.width - segmentWidth) / 2) - outerBezelWidth) - innerBezelWidth
        
        NSLayoutConstraint.activate([
            value5.leftAnchor.constraint(equalTo: leftAnchor, constant: divConstant - 3),
            value5.topAnchor.constraint(equalTo: topAnchor, constant: self.frame.height -  segmentRadius - 33)
        ])
        
        value10.font = valueFont
        value10.textColor = valueColor
        value10.text = "10"
        value10.translatesAutoresizingMaskIntoConstraints = false
        if !self.subviews.contains(value10){
            self.addSubview(value10)
        }
        NSLayoutConstraint.activate([
            value10.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            value10.rightAnchor.constraint(equalTo: rightAnchor, constant: -30),
        ])
        
        needleImage.bounds = CGRect(x: 0, y: 0, width: 15, height: 15)
        needleImage.tintColor = circleUnfillColor
        needleImage.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        
    }
    
    
    //MARK:- Draw Ticks
    func drawTicks(in rect: CGRect, context ctx: CGContext) {
        // save our clean graphics state
        ctx.saveGState()
        ctx.translateBy(x: rect.midX, y: rect.midY * 2)
        ctx.rotate(by: deg2rad(rotation) - (.pi / 2))
        
        let segmentAngle = deg2rad(totalAngle / CGFloat(segmentCount))
        
        let segmentRadius = (((rect.width - segmentWidth) / 2) - outerBezelWidth) - innerBezelWidth
        
        // save the graphics state where we've moved to the center and rotated towards the start of the first segment
        ctx.saveGState()
        
        // draw major ticks
        ctx.setLineWidth(majorTickWidth)
        majorTickColor.set()
        
        let majorEnd = segmentRadius + (segmentWidth / 2)
        let majorStart = majorEnd - majorTickLength
        
        for i in 0 ... segmentCount {
            
            if i != 1{
                ctx.move(to: CGPoint(x: majorStart, y: 0))
                ctx.addLine(to: CGPoint(x: majorEnd, y: 0))
                ctx.drawPath(using: .stroke)
                ctx.rotate(by: segmentAngle)
            }
        }
        
        // go back to the state we had before we drew the major ticks
        ctx.restoreGState()
        
        // save it again, because we're about to draw the minor ticks
        ctx.saveGState()
        
        // draw minor ticks
        ctx.setLineWidth(minorTickWidth)
        minorTickColor.set()
        
        let minorEnd = segmentRadius + (segmentWidth / 2)
        let minorStart = minorEnd - minorTickLength
        
        let minorTickSize = segmentAngle / CGFloat(minorTickCount + 1)
        
        for _ in 0 ..< segmentCount {
            ctx.rotate(by: minorTickSize)
            
            for _ in 0 ..< minorTickCount {
                ctx.move(to: CGPoint(x: minorStart, y: 0))
                ctx.addLine(to: CGPoint(x: minorEnd, y: 0))
                ctx.drawPath(using: .stroke)
                ctx.rotate(by: minorTickSize)
            }
        }
        
        // go back to the graphics state where we've moved to the center and rotated towards the start of the first segment
        ctx.restoreGState()
        
        // go back to the original graphics state
        ctx.restoreGState()
    }
    
    
    
    //MARK:- Draw Semi Circle
    func makeSemiCircle(animationValue : CGFloat) {
        
        let circlePath = UIBezierPath(arcCenter: scaleCenter, radius: radius, startAngle: CGFloat(Double.pi), endAngle: CGFloat(Double.pi * 2), clockwise: true)
        indicatorCircleLayer.removeFromSuperlayer()
        indicatorCircleLayer.path = circlePath.cgPath
        
        indicatorCircleLayer.strokeColor = circleFillColor.cgColor
        indicatorCircleLayer.fillColor = UIColor.clear.cgColor
        indicatorCircleLayer.lineWidth = 7.0
        indicatorCircleLayer.strokeStart = 0
        
        baseSemiCircleLayer.removeFromSuperlayer()
        baseSemiCircleLayer.path = circlePath.cgPath
        baseSemiCircleLayer.strokeColor = circleUnfillColor.cgColor
        baseSemiCircleLayer.fillColor = UIColor.clear.cgColor
        baseSemiCircleLayer.lineWidth = 7.0
        baseSemiCircleLayer.strokeStart = 0
        self.layer.addSublayer(baseSemiCircleLayer)
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = 1.0 //Customize the time of your animation here.
        animation.fromValue = 0.0
        animation.toValue = animationValue
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        
        indicatorCircleLayer.strokeEnd = animationValue
        indicatorCircleLayer.add(animation, forKey: "strokeEndAnimation")
        
        self.layer.addSublayer(indicatorCircleLayer)
        
        setUpUI()
        
        
    }
    
    //MARK:- Set Indicator Helper
    func setIndicator() {
        let angle = (CGFloat(Double.pi * 2) - CGFloat(Double.pi)) * indicatorCircleLayer.strokeEnd + CGFloat(Double.pi)
        let segmentRadius = (((frame.width - (segmentWidth + 7)) / 2) - (outerBezelWidth + 7)) - (innerBezelWidth + 7)
        let point = CGPoint(x: scaleCenter.x + segmentRadius * cos(angle),
                            y: scaleCenter.y + segmentRadius * sin(angle))
        if indicatorValue == 0 || indicatorValue == 1{
            needleImage.removeFromSuperview()
            return
        }
        if !self.subviews.contains(needleImage){
            self.addSubview(needleImage)
        }
        needleImage.center = point
    }
    
    
    //MARK:- Convert degree to radian
    func deg2rad(_ number: CGFloat) -> CGFloat {
        return number * .pi / 180
    }
}
